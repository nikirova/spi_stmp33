﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Devices.Enumeration;
using Windows.Devices.Spi;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace SPI_test
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {

        private SpiDevice STMP33;
        private DispatcherTimer timer;

        public MainPage()
        {
            this.InitializeComponent();
            StartAsync();
        }

        private async Task StartAsync()
        {
            String spiDeviceSelector = SpiDevice.GetDeviceSelector();
            IReadOnlyList<DeviceInformation> devices = await DeviceInformation.FindAllAsync(spiDeviceSelector);

            var STPM33_Settings = new SpiConnectionSettings(0); // 0 = Chip select line to use. 
            STPM33_Settings.ClockFrequency = 4000000;           // 4MHz is the rated speed 
            STPM33_Settings.Mode = SpiMode.Mode3; // We use Mode3 to set the clock polarity and phase to: CPOL = 1, CPHA = 1. [p.69]
            //STPM33_Settings.Mode = SpiMode.Mode0; // We use Mode0 to set the clock polarity and phase to: CPOL = 0, CPHA = 0.

            devices = await DeviceInformation.FindAllAsync(spiDeviceSelector);
            STMP33 = await SpiDevice.FromIdAsync(devices[0].Id, STPM33_Settings);

            // Initialize the STMP33:
            
            // For this device, we create 4-byte write buffers:
            // The first byte is the register address we want to write to.
            // The second byte is the contents that we want to write to the register. 
            
            byte[] xx = new byte[4];

            byte[] setAutoLatch = new byte[] { 0x00, 0x00, 0xFF, 0xFF };
            STMP33.TransferFullDuplex(setAutoLatch, xx);

            //byte[] disableCRC = new byte[] { 0x0e, 0x24, 0x07, 0x00 }; // Register #24 (row 18)
            //STMP33.Write(disableCRC);


            //write FF_FF_FF_FF
            byte[] ww = new byte[] { 0xFF, 0xFF, 0xFF, 0xFF };

            STMP33.TransferFullDuplex(ww, xx);
          

            // Start the polling timer.
            timer = new DispatcherTimer() { Interval = TimeSpan.FromMilliseconds(100) };
            timer.Tick += Timer_Tick;
            timer.Start();
        }

      
        void Timer_Tick(object sender, object e)
        {
           // const byte STMP33_SPI_RW_BIT = 0x80; // Bit used in SPI transactions to indicate read/write
           // const byte STMP33_SPI_MB_BIT = 0x40; // Bit used to indicate multi-byte SPI transactions
          //  const byte STMP33_REG_X = 0x32;      // Address of the X Axis data register

            byte[] ReadBuf = new byte[4];    // Read buffer of size 4 bytes
           
           // byte[] RegAddrBuf = new byte[1 + 6]; // Register address buffer of size 1 byte + 6 bytes padding

            // Register address we want to read from with read and multi-byte bit set
           // RegAddrBuf[0] = STMP33_REG_X | STMP33_SPI_RW_BIT | STMP33_SPI_MB_BIT;

            // If this next line crashes, then there was an error communicating with the device.
            byte[] WriteBuf_RMS = new byte[] { 0x04, 0xFF, 0xFF, 0xFF };
            STMP33.TransferFullDuplex(WriteBuf_RMS, ReadBuf);


            byte[] FF = new byte[] { 0xFF, 0xFF, 0xFF, 0xFF };
            STMP33.TransferFullDuplex(FF, ReadBuf);
        }

        /// Converts the array of 4 bytes into an integer.
        //public static int ConvertToInt(byte[] data)
        //{
        //    int result = 0;
        //    result = data[1] & 0x03;
        //    result <<= 8;
        //    result += data[2];
        //    return result;
        //}

    }
}
